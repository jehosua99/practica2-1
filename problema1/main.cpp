#include <iostream>

using namespace std;
/*
 Al programa se le debera ingresar la cantidad de dinero que se desea devolver, el indicara el numero de billetes y monedas
 minimos que se necesitan, si no es posible devolver exactamente la cantidad necesitada mostrara el faltante.
*/
int salidas(int, int);
int main()
{
    int denominacionbilletes[]= {50000, 20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50}, valor, divisor, cantidad = 0;
    int *p = denominacionbilletes;
    cout << "Ingrese el valor que desea devolver:\n";
    cin >> valor;
    for (int a = 0; a <= 10; a ++)
    {
        divisor = *(p+a);   //Saco el valor almacenado en la posicion "a" del arreglo de datos
        cantidad = valor / divisor;    //Cantidad guardara el numero de billetes necesarios para cada denominacion
        valor %= divisor;  //valor quedara con la cantidad que falta por entregar
        if (a != 10)  //Como la cadena empieza a contar desde 0 en 10 no habra una denominacion de billete o moneda por tanto en esa posicion se imprimira el faltante si lo hay
            cout << denominacionbilletes[a] << ":" << cantidad << endl;
        else
            cout << "Faltante: " << valor << endl;
    }
    return 0;
}

